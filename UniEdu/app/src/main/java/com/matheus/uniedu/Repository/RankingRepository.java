package com.matheus.uniedu.Repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.matheus.uniedu.Models.Ranking;

import java.util.ArrayList;
import java.util.List;

public final class RankingRepository {
    private SQLiteDatabase db;
    private Context context;

    public RankingRepository(Context context){
        this.db = new SQLiteDbHelper(context).getReadableDatabase();
        this.context = context;
    }

    public static class RankingTable implements BaseColumns{
        public final static String TABLE_NAME = "ranking";
        public final static String COLUMN_JOGADORID = "jogadorid";
        public final static String COLUMN_DATA = "data";
        public final static String COLUMN_PONTOS = "pontos";
    }

    public static final String DB_QUERY = "CREATE TABLE " + RankingTable.TABLE_NAME + " ("+
            RankingTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            RankingTable.COLUMN_JOGADORID + " INTEGER REFERENCES " + JogadorRepository.JogadoresTable.TABLE_NAME + "(" + JogadorRepository.JogadoresTable._ID + ")," +
            RankingTable.COLUMN_DATA + " TEXT," +
            RankingTable.COLUMN_PONTOS + " INTEGER)";

    public void salvarRanking(Ranking ranking){
        db.insert(RankingTable.TABLE_NAME, null, getContentValues(ranking));
    }

    private ContentValues getContentValues(Ranking ranking){
        ContentValues cv = new ContentValues();
        cv.put(RankingTable.COLUMN_JOGADORID, ranking.getJogador().getId());
        cv.put(RankingTable.COLUMN_DATA, ranking.getData());
        cv.put(RankingTable.COLUMN_PONTOS, ranking.getPontos());
        return cv;
    }

    public List<Ranking> getRanking(){
        List<Ranking> ranking = new ArrayList<>();
        Cursor cursor = db.query(RankingTable.TABLE_NAME, new String[] { RankingTable.COLUMN_JOGADORID, RankingTable.COLUMN_PONTOS }, null, null, null, null, RankingTable.COLUMN_PONTOS + " DESC");
        if(cursor.moveToFirst()){
            do{
                Ranking rank = new Ranking();
                rank.setJogador(new JogadorRepository(context).getJogador(cursor.getInt(cursor.getColumnIndex(RankingTable.COLUMN_JOGADORID))));
                rank.setPontos(cursor.getInt(cursor.getColumnIndex(RankingTable.COLUMN_PONTOS)));
                ranking.add(rank);
            } while (cursor.moveToNext());
        }
        return ranking;
    }

    public int maiorPontuacao(int jogadorId){
        int valor = 0;
        Cursor cursor = db.rawQuery("SELECT MAX(" + RankingTable.COLUMN_PONTOS + ") FROM " + RankingTable.TABLE_NAME + " WHERE " + RankingTable.COLUMN_JOGADORID + " = " + jogadorId, null);
        cursor.moveToFirst();
        return cursor.getInt(0);
    }
}
