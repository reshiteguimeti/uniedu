package com.matheus.uniedu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.matheus.uniedu.Models.Jogador;
import com.matheus.uniedu.Repository.JogadorRepository;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class CadastroActivity extends AppCompatActivity {

    ImageView imgViewUser;
    EditText edtUsername, edtSenha, edtNome;
    ImageButton btnAddImage;
    Jogador jogador;
    Button btnCadastrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        jogador = new Jogador();

        imgViewUser = findViewById(R.id.imgUsuario);
        edtUsername = findViewById(R.id.edtLoginCadastro);
        edtSenha = findViewById(R.id.edtSenhaCadastro);
        edtNome = findViewById(R.id.edtNomeCadastro);
        btnAddImage = findViewById(R.id.btnImgUsuario);
        btnCadastrar = findViewById(R.id.btnCadastrar);

        edtSenha.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    cadastrar();
                    return true;
                }
                return false;
            }
        });

        btnAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImage();
            }
        });

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastrar();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Cadastro");
    }

    public void cadastrar() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtSenha.getWindowToken(), 0);
        String nome = edtNome.getText().toString(), userName = edtUsername.getText().toString(), senha = edtSenha.getText().toString();
        if (nome.isEmpty()){
            edtNome.setError("O nome não deve estar vazio");
            return;
        }
        if (senha.isEmpty()) {
            edtSenha.setError("A senha não deve estar vazia");
            return;
        }
        if (userName.isEmpty()) {
            edtUsername.setError("O UserName não deve estar vazio");
            return;
        }
        if (senha.length() < 6) {
            edtSenha.setError("A senha deve conter no minímo 6 caracteres");
            return;
        }
        jogador.setUserName(userName);
        jogador.setSenha(senha);
        jogador.setNome(nome);
        JogadorRepository repository = new JogadorRepository(CadastroActivity.this);
        if (!repository.userNameExists(jogador.getUserName())){
            SharedPreferences.Editor editor = getSharedPreferences("uniEduSp", MODE_PRIVATE).edit();
            editor.putInt("idUsuario", repository.saveJogador(jogador));
            editor.putBoolean("logado", true);
            editor.apply();
            startActivity(new Intent(CadastroActivity.this, MainActivity.class));
            finish();
        }
        else
            edtUsername.setError("Já existe um jogador com esse UserName");
    }

    private void addImage(){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                jogador.setImagem(selectedImage);
                imgViewUser.setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(CadastroActivity.this, "Algo deu errado", Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(CadastroActivity.this, "Você não escolheu uma imagem",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(CadastroActivity.this, LoginActivity.class));
                finish();
                break;
            default:break;
        }
        return true;
    }
}
