package com.matheus.uniedu.Models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.matheus.uniedu.R;

import java.util.List;

public class RankingAdapter extends ArrayAdapter<Ranking> {
    private final Context context;
    private final List<Ranking> elementos;

    public RankingAdapter(Context context, List<Ranking> elementos) {
        super (context, R.layout.row_ranking, elementos);
        this.context = context;
        this.elementos = elementos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_ranking, parent, false);

        TextView jogador = (TextView) rowView.findViewById(R.id.txtJogador);
        TextView pontos = (TextView) rowView.findViewById(R.id.txtPontos);
        ImageView trophy = (ImageView) rowView.findViewById(R.id.imgViewRanking);
        ImageView imgJogador = (ImageView) rowView.findViewById(R.id.imgViewJogadorRanking);

        jogador.setText(elementos.get(position).getJogador().getUserName());
        pontos.setText(String.valueOf(elementos.get(position).getPontos()));
        imgJogador.setImageBitmap(elementos.get(position).getJogador().getImagem());

        switch (position){
            case 0:
                trophy.setImageResource(R.drawable.ftrophy);
                break;
            case 1:
                trophy.setImageResource(R.drawable.strophy);
                break;
            case 2:
                trophy.setImageResource(R.drawable.ttrophy);
                break;
            default:
                break;
        }

        return rowView;
    }
}
