package com.matheus.uniedu;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.matheus.uniedu.Models.Jogador;
import com.matheus.uniedu.Models.Questao;
import com.matheus.uniedu.Models.Ranking;
import com.matheus.uniedu.Repository.JogadorRepository;
import com.matheus.uniedu.Repository.QuestaoRepository;
import com.matheus.uniedu.Repository.RankingRepository;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class PartidaActivity extends Activity {

    static final long TIMER = 30000;

    TextView txtPontos, txtQuestao, txtTempo, txtQuestaoCont, txtErros;
    RadioGroup rbGrupo;
    RadioButton rb1, rb2, rb3, rb4;
    Button btnProxima;

    List<Questao> questoes;
    Questao questaoAtual;

    ColorStateList corPadraoRb;
    ColorStateList corPadraoTimer;

    CountDownTimer timer;
    long tempoRestante;

    int questaoAtualCont, totalQuestoes, pontos, erros;

    boolean respondida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partida);

        initialize();
    }

    private void initialize() {
        txtPontos = findViewById(R.id.txtPontos);
        txtQuestao = findViewById(R.id.txtQuestao);
        txtTempo = findViewById(R.id.txtTempo);
        txtQuestaoCont = findViewById(R.id.txtQuestaoCont);
        txtErros = findViewById(R.id.txtErrosCont);

        rbGrupo = findViewById(R.id.radioGroup);

        rb1 = findViewById(R.id.rb1);
        rb2 = findViewById(R.id.rb2);
        rb3 = findViewById(R.id.rb3);
        rb4 = findViewById(R.id.rb4);

        corPadraoRb = rb1.getTextColors();
        corPadraoTimer = txtTempo.getTextColors();

        btnProxima = findViewById(R.id.btnProxima);

        prepararQuestoes();

        proximaQuestao();

        btnProxima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!respondida) {
                    if (rb1.isChecked() || rb2.isChecked() || rb3.isChecked() || rb4.isChecked()) {
                        verificarResposta();
                    } else {
                        Toast.makeText(getApplicationContext(), "Marque uma alternativa", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    proximaQuestao();
                }
            }
        });
    }

    private void proximaQuestao() {
        rb1.setTextColor(corPadraoRb);
        rb2.setTextColor(corPadraoRb);
        rb3.setTextColor(corPadraoRb);
        rb4.setTextColor(corPadraoRb);

        if (questaoAtualCont < totalQuestoes) {
            questaoAtual = questoes.get(questaoAtualCont);

            txtQuestao.setText(questaoAtual.getDescricao());

            rbGrupo.clearCheck();

            rb1.setText(questaoAtual.getAlternativa1());
            rb2.setText(questaoAtual.getAlternativa2());
            rb3.setText(questaoAtual.getAlternativa3());
            rb4.setText(questaoAtual.getAlternativa4());

            questaoAtualCont++;

            txtQuestaoCont.setText("Questão " + questaoAtualCont + " de " + totalQuestoes);

            respondida = false;

            btnProxima.setText("Confirmar");

            tempoRestante = TIMER;

            iniciarTimer();

        } else {
            finalizarPartida();
        }
    }

    private void iniciarTimer() {
        timer = new CountDownTimer(tempoRestante, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tempoRestante = millisUntilFinished;
                atualizarTextoTimer();
            }

            @Override
            public void onFinish() {
                tempoRestante = 0;
                atualizarTextoTimer();
                verificarResposta();
            }
        }.start();
    }

    private void atualizarTextoTimer() {
        int minutos = (int) (tempoRestante / 1000) / 60;
        int segundos = (int) (tempoRestante / 1000) % 60;

        String tempoFormatado = String.format(Locale.getDefault(), "%02d:%02d", minutos, segundos);

        txtTempo.setText(tempoFormatado);

        if (tempoRestante < 6000)
            txtTempo.setTextColor(Color.RED);
        else
            txtTempo.setTextColor(corPadraoTimer);
    }

    private void verificarResposta() {
        respondida = true;

        timer.cancel();

        RadioButton rbSelecionada = findViewById(rbGrupo.getCheckedRadioButtonId());
        int resposta = rbGrupo.indexOfChild(rbSelecionada) + 1;

        if (resposta == questaoAtual.getQuestaoCerta()) {
            pontos += questaoAtual.getPontos();

            txtPontos.setText("Pontos: " + pontos);
        }
        else if (erros >= 3)
            finalizarPartida();
        else{
            erros += 1;

            txtErros.setText("Erros: " + erros + " de 3");
        }

        mostrarResposta();
    }

    private void mostrarResposta() {
        rb1.setTextColor(Color.RED);
        rb2.setTextColor(Color.RED);
        rb3.setTextColor(Color.RED);
        rb4.setTextColor(Color.RED);

        RadioButton rbCerta = (RadioButton) rbGrupo.getChildAt(questaoAtual.getQuestaoCerta() - 1);
        rbCerta.setTextColor(Color.GREEN);

        if (questaoAtualCont < totalQuestoes)
            btnProxima.setText("Próxima");
        else
            btnProxima.setText("Finalizar");
    }

    private void finalizarPartida() {
        if (pontos != 0) {
            SharedPreferences sp = getSharedPreferences("uniEduSp", MODE_PRIVATE);
            Jogador jogador = new JogadorRepository(PartidaActivity.this).getJogador(sp.getInt("idUsuario", 0));
            Ranking ranking = new Ranking();
            ranking.setJogador(jogador);
            ranking.setPontos(pontos);
            new RankingRepository(PartidaActivity.this).salvarRanking(ranking);
            Intent intent = new Intent(PartidaActivity.this, GameOverActivity.class);
            intent.putExtra("jogadorId", jogador.getId());
            intent.putExtra("pontos", pontos);
            startActivity(intent);
        }
        finish();
    }

    private void prepararQuestoes() {
        SharedPreferences sharedPrefs = getSharedPreferences("uniEduSp", MODE_PRIVATE);
        int nivel = sharedPrefs.getInt("nivel", 0) + 1;
        int tema = sharedPrefs.getInt("tema", 0);
        questoes = new QuestaoRepository(PartidaActivity.this).getQuestoes(nivel, tema);
        totalQuestoes = questoes.size();
        Collections.shuffle(questoes);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Cancelar");

        builder.setMessage("Deseja cancelar a partida?");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                finalizarPartida();
            }
        });

        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                return;
            }
        });

        AlertDialog alerta = builder.create();

        alerta.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null)
            timer.cancel();
    }
}
