package com.matheus.uniedu;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences sharedPrefs = getSharedPreferences("uniEduSp", MODE_PRIVATE);

                if (sharedPrefs.getBoolean("logado", false)) {
                    startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                }
                else
                    startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
                finish();
            }
        }, 2000);
    }
}
