package com.matheus.uniedu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.matheus.uniedu.Models.Jogador;
import com.matheus.uniedu.Repository.JogadorRepository;
import com.matheus.uniedu.Repository.RankingRepository;

public class PerfilActivity extends AppCompatActivity {

    ImageView imgPerfil;
    TextView txtNomePerfil, txtUserName, txtMaiorPontuacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Perfil");

        txtNomePerfil = findViewById(R.id.txtNomePerfil);
        txtUserName = findViewById(R.id.txtUserNamePerfil);
        txtMaiorPontuacao = findViewById(R.id.txtMaiorPontuacaoPerfil);
        imgPerfil = findViewById(R.id.imgPerfil);

        int jogadorId = getIntent().getIntExtra("jogadorId", 0);

        Jogador jogador = new JogadorRepository(PerfilActivity.this).getJogador(jogadorId);
        txtNomePerfil.setText(jogador.getNome());
        txtUserName.setText(jogador.getUserName());
        txtMaiorPontuacao.setText(String.valueOf(new RankingRepository(PerfilActivity.this).maiorPontuacao(jogadorId)));
        if (jogador.getImagem() == null)
            imgPerfil.setImageResource(R.drawable.ic_person_black_24dp);
        else
            imgPerfil.setImageBitmap(jogador.getImagem());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:break;
        }
        return true;
    }
}
