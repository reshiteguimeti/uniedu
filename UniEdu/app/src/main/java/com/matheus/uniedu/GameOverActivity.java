package com.matheus.uniedu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.matheus.uniedu.Models.Jogador;
import com.matheus.uniedu.Repository.JogadorRepository;
import com.matheus.uniedu.Repository.RankingRepository;

public class GameOverActivity extends AppCompatActivity {

    ImageView imgJogador;
    TextView txtUserName, txtPontuacao, txtPontuacaoMaxima;
    Button btnOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        imgJogador = findViewById(R.id.imgPerfilGameOver);
        txtUserName = findViewById(R.id.txtUserNameGameOver);
        txtPontuacao = findViewById(R.id.txtPontuacaoGameOver);
        txtPontuacaoMaxima = findViewById(R.id.txtMaiorPontuacaoGameOver);
        btnOk = findViewById(R.id.btnOkGameOver);

        int jogadorId = getIntent().getIntExtra("jogadorId", 0);
        int pontos = getIntent().getIntExtra("pontos", 0);
        int maiorPontuacao = new RankingRepository(GameOverActivity.this).maiorPontuacao(jogadorId);

        Jogador jogador = new JogadorRepository(GameOverActivity.this).getJogador(jogadorId);
        txtUserName.setText(jogador.getUserName());
        txtPontuacaoMaxima.setText(String.valueOf(maiorPontuacao));
        txtPontuacao.setText(String.valueOf(pontos));
        if (jogador.getImagem() == null)
            imgJogador.setImageResource(R.drawable.ic_person_black_24dp);
        else
            imgJogador.setImageBitmap(jogador.getImagem());

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GameOverActivity.this, MainActivity.class));
                finish();
            }
        });
    }
}
