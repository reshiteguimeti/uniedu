package com.matheus.uniedu.Repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.matheus.uniedu.Models.Questao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class QuestaoRepository {

    private SQLiteDatabase db;

    public QuestaoRepository(Context context) {
        this.db = new SQLiteDbHelper(context).getReadableDatabase();
    }

    public static class QuestoesTable implements BaseColumns {
        public final static String TABLE_NAME = "questoes";
        public final static String COLUMN_DESCRICAO = "descricao";
        public final static String COLUMN_ALTERNATIVA1 = "alternativa1";
        public final static String COLUMN_ALTERNATIVA2 = "alternativa2";
        public final static String COLUMN_ALTERNATIVA3 = "alternativa3";
        public final static String COLUMN_ALTERNATIVA4 = "alternativa4";
        public final static String COLUMN_QUESTAO_CERTA = "questao_certa";
        public final static String COLUMN_PONTOS = "pontos";
        public final static String COLUMN_NIVEL = "nivel";
        public final static String COLUMN_TEMA = "tema";
    }

    public static final String DB_QUERY = "CREATE TABLE " + QuestoesTable.TABLE_NAME + " (" +
                             QuestoesTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                             QuestoesTable.COLUMN_DESCRICAO + " TEXT, " +
                             QuestoesTable.COLUMN_ALTERNATIVA1 + " TEXT, " +
                             QuestoesTable.COLUMN_ALTERNATIVA2 + " TEXT, " +
                             QuestoesTable.COLUMN_ALTERNATIVA3 + " TEXT, " +
                             QuestoesTable.COLUMN_ALTERNATIVA4 + " TEXT, " +
                             QuestoesTable.COLUMN_QUESTAO_CERTA + " INTEGER, " +
                             QuestoesTable.COLUMN_PONTOS + " INTEGER, " +
                             QuestoesTable.COLUMN_NIVEL + " INTEGER, " +
                             QuestoesTable.COLUMN_TEMA + " INTEGER)";

    public static final List<ContentValues> seedQuestoes(){
        List<Questao> questoes = new ArrayList<>(Arrays.asList(
                //Android
                new Questao("São componentes independentes que representam as interfaces do seu aplicativo. Isso se refere à?", "Activity", "mipmap", "Layout", "drawable", 1, 10, 1, 1),
                new Questao("Qual é o recurso que o Android dispõe para facilitar a edição de String no aplicativo?", "string.xml", "AndroidManifest.xml", "colors.xml", "styles.xml", 1, 10, 1, 1),
                new Questao("Qual é o recurso que o Android dispõe para facilitar a edição de cores no aplicativo?", "AndroidManifest.xml", "colors.xml", "styles.xml", "AndroidManifest.xml", 2, 10, 1, 1),
                new Questao("São componentes independentes que representam as interfaces gráficas do seu aplicativo. Isso se refere à?", "Activity", "mipmap", "Layout", "drawable", 3, 10, 1, 1),
                new Questao("Qual pasta deve guardar as imagem e icones do seu aplicativo?", "Layout", "drawable", "Activity", "mipmap", 2, 10, 1, 1),
                new Questao("Qual é o metodo que captura um elemento do seu layout?", "findViewById();", "getById();", "layoutById();", "SetById();", 1, 10, 1, 1),
                new Questao("Qual é o elemento que mostra um texto na tela?", "TextView", "Spinner", "Button", "EditText", 1, 10, 1, 1),
                new Questao("Qual é o elemento em que o usuário digita um texto?", "Button", "TextView", "Spinner", "EditText", 4, 10, 1, 1),
                new Questao("Qual é o elemento que representa um botão?", "EditText", "Button", "Spinner", "TextView", 2, 10, 1, 1),
                new Questao("Qual é o elemento que representa um DropDown?", "EditText", "TextView", "Spinner", "EditText", 3, 10, 3, 1),
                new Questao("Qual desses NÃO representa um método do ciclo de vida de uma activity?", "OnCreation();", "OnStart();", "OnDestroy();", "OnStop();", 1, 10, 3, 1),
                new Questao("Qual é o arquivo que apresenta informações essenciais sobre o aplicativo ao sistema Android, necessárias para o sistema antes que ele possa executar o código do aplicativo?", "String.xml", "AndroidManifest.xml", "colors.xml", "styles.xml", 2, 10, 1, 1),
                new Questao("Linguagem nativa da plataforma, instalação a partir das lojas store. Qual método de desenvolvimento isto se refere?", "App Hibrido", "Mobile Site", "App Nativo", "Site Reponsivo", 3, 20, 2, 1),
                new Questao("HTML5 Css3 JavaScript, nas plataformas populares, instalação a partir das lojas store. Qual método de desenvolvimento isto se refere?", "Mobile Site", "App Hibrido", "App Nativo", "Site Reponsivo", 2, 20, 2, 1),
                new Questao("HTML5 Css3 JavaScript, acessado a partir do Browser, projeto separado, código duplicado. Qual método de desenvolvimento isto se refere?", "App Hibrido", "Mobile Site", "App Nativo", "Site Reponsivo", 2, 20, 2, 1),
                new Questao("HTML5 Css3 JavaScript, acessado a partir do Browser, 'responde' para tamanho de telas diferentes, ideal para sites informacionais etc. Qual método de desenvolvimento isto se refere?", "App Nativo", "Site Reponsivo", "Mobile Site", "App Hibrido", 2, 20, 2, 1),

                //C#
                new Questao("Qual expressão representa uma lambda no C#?", "->", ">>", ">", "=>", 4, 10, 1, 2),
                new Questao("Na programação multithread, qual dos seguintes não está usando o pool de Thread?", "BackgroundWorker class", "Asynchronous delegate", "Thread class", "Task class", 4, 20, 2, 2),
                new Questao("No seguinte exemplo C#, a variável \"a\" é string. Encontre um que esteja errado ou retorne um resultado diferente.", "a = a ?? \"\";", "a = a == null? \"\" : a;", "a = (a is null)? \"\" : a;", "if (a == null) a = \"\";", 3, 20, 2, 2),
                new Questao("Encontre um exemplo inválido de uso do var no C#", "var a = 3,141592;", "var a = null;", "var a = db.Stores;", "var a = db.Stores.Single(p => p.Id == 1);", 2, 10, 1, 2),
                new Questao("Qual dessas alternativas declara corretamente um array inteiro bidimensional em C#?", "Int [] [] myArray;", "Int [2] myArray;", "System.Array [2] myArray;", "Int [,] myArray;", 4, 20, 2, 2),
                new Questao("Encontre uma expressão inválida entre os seguintes exemplos de genéricos do C#", "class A where T : class, new()", "class A where T : struct, IComparable","class A where T : class, struct", "class A where T : Stream where U : IDisposable", 3, 30, 3, 2),
                new Questao("Qual das seguintes palavras-chave do C# não tem nada a ver com multithreading?", "sealed", "async","lock", "await", 1, 10, 1, 2),
                new Questao("Encontrar um protótipo do método Main() inválido, que é o ponto de entrada em C#?", "public static void Main()", "public static int Main()","public static void Main(string[] s)", "public static long Main(string[] args)", 4, 30, 2, 2),
                new Questao("No exemplo abaixo, button1 é um objeto da classe Button no WinForms. Qual deles é uma expressão errada como um manipulador de eventos de clique?", "button1.Click += new System.EventHandler(button1_Click);", "button1.Click += delegate { MessageBox.Show(\"Click\");};", "button1.Click += delegate(EventArgs e){MessageBox.Show(\"Click\");};", "button1.Click += (s, e) => MessageBox.Show(\"Click\");", 3, 30, 3, 2),

                //SQL
                new Questao("Qual a função para obter o maior valor em uma busca?", "Max", "Sum", "Count", "Avg", 1, 10, 1, 3),
                new Questao("É executado sempre que ocorre uma ação definida pelo DBA", "Procedure", "Function", "Query", "Trigger", 4, 20, 2, 3),
                new Questao("Insert é da classe de comandos...", "DDL", "DQL", "DTL", "DML", 4, 30, 3, 3),
                new Questao("Qual afirmação é uma declaração de recuperação de dados no SQL?", "Insert", "Select", "Update", "Merge", 2, 10, 1, 3),
                new Questao("Qual destas é uma instrução de controle de transação no SQL?", "Alter", "Rename", "Truncate", "Savepoint", 4, 30, 3, 3),
                new Questao("O que significa iSQL?", "Interface of Structured Query Language", "Intermediate Structured Query Language", "Informative Structured Query Language", "Interactive Structured Query Language", 4, 20, 2, 3),
                new Questao("Qual dos seguintes NÃO é um tipo de join usado em SQL?", "INNER", "OUTER", "EXTRA", "CROSS", 3, 10, 1, 3),
                new Questao("Para obter todos os dados de uma tabela de nome tblProdutos onde os nomes dos produtos, que estão na coluna NomeProduto, se iniciam com a letra M. Qual query usar?", "SELECT * FROM tblProdutos WHERE NomeProduto LIKE 'M%'", "SELECT $ FROM tblProdutos WHERE NomeProduto LIKE 'M%'", "SELECT * FROM tblProdutos WHERE NomeProduto = '%M'", "SELECT * FROM NomeProduto WHERE tblProdutos = '%M'", 1, 10, 1, 3),
                new Questao("Qual a função da cláusula HAVING?", "Usamos a cláusula HAVING para unir duas tabelas em uma consulta", "Usamos a cláusula HAVING para especificar condições de filtragem em grupos de registros", "Usamos a cláusula HAVING para substituir a cláusula WHERE nas consultas", "Usamos a cláusula HAVING para retornar registros que iniciem com uma letra específica", 2, 30, 3, 3),
                new Questao("Para que serve a função AVG?", "Efetuar backup do banco de dados completo", "Executar um antivírus no banco de dados, para segurança", "Calcular a média aritmética em um conjunto de dados", "Ordenar os dados em uma consulta em ordem alfabética", 3, 20, 2, 3),
                new Questao("Qual a declaração que devemos utilizar para retornar os dados da coluna NomeCliente da tabela tblClientes?", "RETURN NomeCliente FROM tblClientes", "SELECT tblClientes FROM NomeCliente", "GET NomeCliente BY tblClientes", "SELECT NomeCliente FROM tblClientes", 4, 10, 1, 3)
        ));
       return fillQuestoes(questoes);
    }

    private static final List<ContentValues> fillQuestoes(List<Questao> questoes){
        List<ContentValues> contentValues = new ArrayList<>();

        for (Questao questao : questoes) {
            ContentValues cv = new ContentValues();
            cv.put(QuestoesTable.COLUMN_DESCRICAO, questao.getDescricao());
            cv.put(QuestoesTable.COLUMN_ALTERNATIVA1, questao.getAlternativa1());
            cv.put(QuestoesTable.COLUMN_ALTERNATIVA2, questao.getAlternativa2());
            cv.put(QuestoesTable.COLUMN_ALTERNATIVA3, questao.getAlternativa3());
            cv.put(QuestoesTable.COLUMN_ALTERNATIVA4, questao.getAlternativa4());
            cv.put(QuestoesTable.COLUMN_QUESTAO_CERTA, questao.getQuestaoCerta());
            cv.put(QuestoesTable.COLUMN_PONTOS, questao.getPontos());
            cv.put(QuestoesTable.COLUMN_NIVEL, questao.getNivel());
            cv.put(QuestoesTable.COLUMN_TEMA, questao.getTema());
            contentValues.add(cv);
        }

        return contentValues;
    }

    public List<Questao> getQuestoes(int nivel, int tema){
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM " + QuestoesTable.TABLE_NAME);
        builder.append(" WHERE " + QuestoesTable.COLUMN_NIVEL + " <= " + nivel);
        if (tema != 0)
            builder.append(" AND " + QuestoesTable.COLUMN_TEMA + " = " + tema);
        return preencherQuestoes(this.db.rawQuery(builder.toString(), null));
    }

    private List<Questao> preencherQuestoes(Cursor cursor){
        List<Questao> questoes = new ArrayList<>();
        if (cursor.moveToFirst()){
            do {
                Questao questao = new Questao();
                questao.setDescricao(cursor.getString(cursor.getColumnIndex(QuestoesTable.COLUMN_DESCRICAO)));
                questao.setAlternativa1(cursor.getString(cursor.getColumnIndex(QuestoesTable.COLUMN_ALTERNATIVA1)));
                questao.setAlternativa2(cursor.getString(cursor.getColumnIndex(QuestoesTable.COLUMN_ALTERNATIVA2)));
                questao.setAlternativa3(cursor.getString(cursor.getColumnIndex(QuestoesTable.COLUMN_ALTERNATIVA3)));
                questao.setAlternativa4(cursor.getString(cursor.getColumnIndex(QuestoesTable.COLUMN_ALTERNATIVA4)));
                questao.setQuestaoCerta(cursor.getInt(cursor.getColumnIndex(QuestoesTable.COLUMN_QUESTAO_CERTA)));
                questao.setPontos(cursor.getInt(cursor.getColumnIndex(QuestoesTable.COLUMN_PONTOS)));
                questao.setNivel(cursor.getInt(cursor.getColumnIndex(QuestoesTable.COLUMN_NIVEL)));
                questao.setTema(cursor.getInt(cursor.getColumnIndex(QuestoesTable.COLUMN_TEMA)));
                questoes.add(questao);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return questoes;
    }
}
