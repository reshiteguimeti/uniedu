package com.matheus.uniedu.Repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.matheus.uniedu.Repository.QuestaoRepository.QuestoesTable;

public class SQLiteDbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "uniedu";
    private static final int DB_VERSION = 1;

    private SQLiteDatabase db;

    public SQLiteDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;
        db.execSQL(JogadorRepository.DB_QUERY);
        db.execSQL(QuestaoRepository.DB_QUERY);
        db.execSQL(RankingRepository.DB_QUERY);
        for (ContentValues cv: QuestaoRepository.seedQuestoes()) {
            db.insert(QuestoesTable.TABLE_NAME, null, cv);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + QuestoesTable.TABLE_NAME);
        onCreate(db);
    }
}
