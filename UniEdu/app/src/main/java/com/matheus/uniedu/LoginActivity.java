package com.matheus.uniedu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.matheus.uniedu.Repository.JogadorRepository;

public class LoginActivity extends AppCompatActivity {

    EditText edtLogin, edtSenha;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtLogin = findViewById(R.id.edtUserLogin);
        edtSenha = findViewById(R.id.edtUserSenha);
        btnLogin = findViewById(R.id.btnLogin);

        edtSenha.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    logar();
                    return true;
                }
                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logar();
            }
        });
    }

    public void logar() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtSenha.getWindowToken(), 0);
        String login = edtLogin.getText().toString(), senha = edtSenha.getText().toString();
        if (login.isEmpty()){
            edtLogin.setError("O UserName não deve estar vazio");
            return;
        }
        if (senha.isEmpty()){
            edtSenha.setError("A senha não deve estar vazia");
            return;
        }
        JogadorRepository repository = new JogadorRepository(LoginActivity.this);
        if (repository.jogadorExists(login, senha)){
            SharedPreferences.Editor editor = getSharedPreferences("uniEduSp", MODE_PRIVATE).edit();
            editor.putInt("idUsuario", repository.getJogadorId(edtLogin.getText().toString(), edtSenha.getText().toString()));
            editor.putBoolean("logado", true);
            editor.apply();
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }
        else
            Toast.makeText(LoginActivity.this, "Jogador não cadastrado", Toast.LENGTH_LONG).show();
    }

    public void goToCadastro(View view) {
        startActivity(new Intent(LoginActivity.this, CadastroActivity.class));
        finish();
    }
}
