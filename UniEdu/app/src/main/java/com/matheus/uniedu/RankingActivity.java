package com.matheus.uniedu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.matheus.uniedu.Models.Ranking;
import com.matheus.uniedu.Models.RankingAdapter;
import com.matheus.uniedu.Repository.RankingRepository;

import java.util.List;

public class RankingActivity extends AppCompatActivity {

    ListView list_ranking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        list_ranking = findViewById(R.id.list_ranking);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Ranking");

        final List<Ranking> rankingList = new RankingRepository(RankingActivity.this).getRanking();
        ArrayAdapter<Ranking> rankingAdapter = new RankingAdapter(getApplicationContext(), rankingList);

        list_ranking.setAdapter(rankingAdapter);

        list_ranking.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(RankingActivity.this, PerfilActivity.class);
                intent.putExtra("jogadorId", rankingList.get(position).getJogador().getId());
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:break;
        }
        return true;
    }
}
