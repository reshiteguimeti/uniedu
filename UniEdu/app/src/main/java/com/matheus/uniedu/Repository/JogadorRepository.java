package com.matheus.uniedu.Repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.matheus.uniedu.Models.ImageHelper;
import com.matheus.uniedu.Models.Jogador;

public final class JogadorRepository {

    private SQLiteDatabase db;

    public JogadorRepository(Context context){
        this.db = new SQLiteDbHelper(context).getReadableDatabase();
    }

    public static class JogadoresTable implements BaseColumns {
        public final static String TABLE_NAME = "jogadores";
        public final static String COLUMN_USERNAME = "username";
        public final static String COLUMN_NOME = "nome";
        public final static String COLUMN_SENHA = "senha";
        public final static String COLUMN_IMAGEM = "imagem";
    }

    public static final String DB_QUERY = "CREATE TABLE " + JogadoresTable.TABLE_NAME + " (" +
            JogadoresTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            JogadoresTable.COLUMN_USERNAME + " TEXT, " +
            JogadoresTable.COLUMN_NOME + " TEXT, " +
            JogadoresTable.COLUMN_SENHA + " TEXT, " +
            JogadoresTable.COLUMN_IMAGEM + " BLOB)";

    public int saveJogador(Jogador jogador){
        db.insert(JogadoresTable.TABLE_NAME, null, getContentValues(jogador));
        Cursor cursor = db.rawQuery("SELECT last_insert_rowid() FROM " + JogadoresTable.TABLE_NAME, null);
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    private ContentValues getContentValues(Jogador jogador){
        ContentValues cv = new ContentValues();
        cv.put(JogadoresTable.COLUMN_USERNAME, jogador.getUserName());
        cv.put(JogadoresTable.COLUMN_NOME, jogador.getNome());
        cv.put(JogadoresTable.COLUMN_SENHA, jogador.getSenha());
        if (jogador.getImagem() != null)
            cv.put(JogadoresTable.COLUMN_IMAGEM, ImageHelper.getBitmapAsByteArray(jogador.getImagem()));
        return cv;
    }

    public Jogador getJogador(int id){
        Jogador jogador = new Jogador();
        Cursor cursor = db.rawQuery("SELECT * FROM " + JogadoresTable.TABLE_NAME + " WHERE _ID = " + id, null);
        if (cursor.moveToFirst()){
            jogador.setId(cursor.getInt(cursor.getColumnIndex(JogadoresTable._ID)));
            jogador.setNome(cursor.getString(cursor.getColumnIndex(JogadoresTable.COLUMN_NOME)));
            jogador.setUserName(cursor.getString(cursor.getColumnIndex(JogadoresTable.COLUMN_USERNAME)));
            byte[] imgArray = cursor.getBlob(cursor.getColumnIndex(JogadoresTable.COLUMN_IMAGEM));
            if (imgArray != null)
                jogador.setImagem(ImageHelper.getBitmapFromByteArray(imgArray));
        }
        return jogador;
    }

    public int getJogadorId(String userName, String senha){
        Cursor cursor = db.rawQuery("SELECT * FROM " + JogadoresTable.TABLE_NAME +
                " WHERE " + JogadoresTable.COLUMN_USERNAME + " = ? " +
                " AND " + JogadoresTable.COLUMN_SENHA + " = ? ", new String[] {userName, senha});
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public boolean jogadorExists(String userName, String senha) {
        Cursor cursor = db.rawQuery("SELECT * FROM " + JogadoresTable.TABLE_NAME +
                                        " WHERE " + JogadoresTable.COLUMN_USERNAME + " = ? " +
                                        " AND " + JogadoresTable.COLUMN_SENHA + " = ? ", new String[] {userName, senha});
        return cursor.moveToFirst();
    }

    public boolean userNameExists(String userName){
        Cursor cursor = db.rawQuery("SELECT * FROM " + JogadoresTable.TABLE_NAME +
                " WHERE " + JogadoresTable.COLUMN_USERNAME + " = ? ", new String[] {userName});
        return cursor.moveToFirst();
    }
}
