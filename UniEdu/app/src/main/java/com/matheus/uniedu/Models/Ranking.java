package com.matheus.uniedu.Models;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Ranking {
    private Jogador jogador;
    private String data;
    private int pontos;

    public Ranking(){
        SimpleDateFormat formataData = new SimpleDateFormat("dd-MM-yyyy");
        Date dataT = new Date();
        data = formataData.format(dataT);
    }

    public Jogador getJogador() {
        return jogador;
    }

    public void setJogador(Jogador jogador) {
        this.jogador = jogador;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public String getData() {
        return data;
    }
}
