package com.matheus.uniedu;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConfiguracaoActivity extends AppCompatActivity {

    AppCompatSpinner spinnerNivel, spinnerTemas;
    Button btnSalvarConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracao);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Configurações");

        spinnerNivel = findViewById(R.id.spinnerNivel);
        spinnerTemas = findViewById(R.id.spinnerTemas);
        btnSalvarConfig = findViewById(R.id.btnSalvarConfig);

        List<String> niveisList = new ArrayList<>(Arrays.asList("Fácil", "Médio", "Difícil"));
        ArrayAdapter<String> niveisAdapter = new ArrayAdapter<>(ConfiguracaoActivity.this, android.R.layout.simple_dropdown_item_1line, niveisList);
        spinnerNivel.setAdapter(niveisAdapter);

        ArrayAdapter<String> temasAdapter = new ArrayAdapter<>(ConfiguracaoActivity.this, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.temas));
        spinnerTemas.setAdapter(temasAdapter);

        setDefaultSelection();

        btnSalvarConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getSharedPreferences("uniEduSp", MODE_PRIVATE).edit();
                editor.putInt("nivel", spinnerNivel.getSelectedItemPosition());
                editor.putInt("tema", spinnerTemas.getSelectedItemPosition());
                editor.apply();
                finish();
            }
        });
    }

    private void setDefaultSelection(){
        SharedPreferences sp = getSharedPreferences("uniEduSp", MODE_PRIVATE);

        spinnerNivel.setSelection(sp.getInt("nivel", 0));
        spinnerTemas.setSelection(sp.getInt("tema", 0));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:break;
        }
        return true;
    }
}
